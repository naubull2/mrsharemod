/*
 * Project : MRShareMod
 * Module : MRShareMod
 * Class : TaggedKeyPair
 *
 * Author : Wooin Lee
 * Last edited : 15. 6. 23 ���� 11:24
 * wilee@kdd.snu.ac.kr
 */

package Utils;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.io.WritableUtils;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class TaggedKeyPair implements WritableComparable {
    private byte tag;
    private String value;

    public TaggedKeyPair(){}

    public TaggedKeyPair(byte tag, String value) {
        set(tag, value);
    }

    public void set(byte tag, String value) {
        this.value = value;
        this.tag = tag;
    }
    public void set(String value){  this.value = value;  }
    public void set(byte tag){ this.tag = tag; }

    public String getValue() {
        return value;
    }

    public byte getTag() {
        return tag;
    }

    @Override
    public int hashCode() {
        int a = Integer.parseInt(value) * 3571 % 6301;
        a = (a+0x7ed55d16) + (a<<12);
        a = (a^0xc761c23c) ^ (a>>19);
        a = (a+0x165667b1) + (a<<5);
        a = (a+0xd3a2646c) ^ (a<<9);
        a = (a+0xfd7046c5) + (a<<3);
        a = (a^0xb55a4f09) ^ (a>>16);
        return a;
    }

    @Override
    public void write(DataOutput out) throws IOException {
        out.writeByte(tag);
        out.writeBytes(value);
    }

    @Override
    public void readFields(DataInput in) throws IOException {
        tag = in.readByte();
        value = in.readLine();
    }

    @Override
    public String toString(){
        // (byte)(int)Integer.valueOf(integer, 2) this will convert string representation of byte to byte
        return Integer.toBinaryString((tag & 0xFF) + 0x100).substring(1) + "\t" + value;
    }

    @Override
    public int compareTo(Object o) {
        TaggedKeyPair other = (TaggedKeyPair) o;
        return value.compareTo(other.getValue());
    }

    @Override
    public boolean equals(Object o){
        if(!(o instanceof TaggedKeyPair)) return false;
        TaggedKeyPair other = (TaggedKeyPair)o;
        return (this.value.equals(other.getValue()) && this.tag == other.getTag());
    }

    /**
    private int byteCompare(byte[] left, byte[] right) {
        for (int i = 0, j = 0; i < left.length && j < right.length; i++, j++) {
            int a = (left[i] & 0xff);
            int b = (right[j] & 0xff);
            if (a != b) {
                return a - b;
            }
        }
        return left.length - right.length;
    }
     **/
    /**
     * performance comparison should be done on this
     * 1. with the class method compareTo()
     * 2. with the static class KeyComparator

    public static class KeyComparator extends WritableComparator {
        public KeyComparator(){ super(Text.class);}
        public int compare(byte[] b1, int s1, int l1, byte[] b2, int s2, int l2) {
            int n1 = WritableUtils.decodeVIntSize(b1[s1]);
            int n2 = WritableUtils.decodeVIntSize(b2[s2]);
            return compareBytes(b1, s1+n1, l1-n1, b2, s2+n2, l2-n2);
        }
    }
    static{ // register this comparator
        WritableComparator.define(TaggedKeyPair.class, new KeyComparator());
    }
    */

}
