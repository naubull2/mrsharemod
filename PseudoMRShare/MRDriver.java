/*
 * Project : MRShareMod
 * Module : MRShareMod
 * Class : MRDriver
 *
 * Author : Wooin Lee
 * Last edited : 15. 6. 23 ���� 11:05
 * wilee@kdd.snu.ac.kr
 */

package PseudoMRShare;

import Utils.TaggedKeyPair;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class MRDriver {
    private static final int numReducers = 1;
    /**
     * 1. input file
     * 2. output file
     * 3. number of expressions
     * 4. regular expressions separated by white spaces
     */
    public static void main(String[] args) throws Exception{
        Configuration conf = new Configuration();
        String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
        if(otherArgs.length < 4){
            printHelp();
            return;
        }

        Path input = new Path(otherArgs[0]);
        Path output;

        int numRegex = Integer.parseInt(otherArgs[2]);
        String[] regex = new String[numRegex];
        for(int i = 0; i < numRegex; i++) regex[i] = otherArgs[3+i];

        /********************************
         * Baseline running multiple mapreduce jobs in sequential order
         */
        long startTime = System.currentTimeMillis();
        for(int i = 0; i<numRegex; i++) {
            String[] baseline = new String[1];
            baseline[0] = regex[i];
            output = new Path(otherArgs[1]+"base"+i);
            runMapreduce(conf, input, output, "base", numReducers, 1, baseline);
        }
        long endTime = System.currentTimeMillis();
        float tBASELINE= (endTime-startTime)/1000.0f;

        /********************************
         * Merged mapreduce jobs using (key + tag) method used in MRShare
         */
        startTime = System.currentTimeMillis();
        output = new Path(otherArgs[1]+"mrshare");
        runMapreduce(conf, input, output, "mrshare", numReducers, numRegex, regex);
        endTime = System.currentTimeMillis();
        float tMRSHARE = (endTime-startTime)/1000.0f;

        /*********************************
         * Merged mapreduce jobs using only a custom partitioner
         */
        startTime = System.currentTimeMillis();
        output = new Path(otherArgs[1]+"partitioner");
        runMapreduce(conf, input, output, "partitioner", numReducers, numRegex, regex);
        endTime = System.currentTimeMillis();
        float tPARTITIONER = (endTime-startTime)/1000.0f;


        System.out.println("*******************************************************");
        System.out.println("Baseline algorithm :\t"     + tBASELINE +"secs");
        System.out.println("MRShare algorithm :\t"      + tMRSHARE +"secs");
        System.out.println("Partitioner algorithm :\t"  + tPARTITIONER +"secs");
        System.out.println("*******************************************************");
    }
    /**
     * Runs single grep-wordcount mapreduce phase
     * @param config Configuration object
     * @param inputDir Input path
     * @param outputDir Output path
     * @param runHeader Header for indicating which phase this is
     * @param numReducers Number of reducers available
     * @param numRegex Number of regular expressions (jobs)
     * @param regex String array of regular expressions
     * @throws Exception
     */
    private static void runMapreduce(Configuration config, Path inputDir, Path outputDir, String runHeader, int numReducers, int numRegex, String[] regex) throws Exception{
        // broadcast regex
        config.set("header", runHeader);
        config.setInt("numRegex", numRegex);
        for(int i = 0; i < numRegex; i++)
            config.set("regex."+i, regex[i]);

        FileSystem fs = FileSystem.get(config);
        Job job = Job.getInstance(config, "pseudo MRShare");
        job.setJarByClass(MRDriver.class);

        job.setMapperClass(GrepMapper.class);
        job.setReducerClass(GrepReducer.class);
        // Combiner ?

        job.setMapOutputKeyClass(TaggedKeyPair.class);
        job.setMapOutputValueClass(IntWritable.class);

        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(IntWritable.class);

        job.setNumReduceTasks(numReducers);

        if(fs.exists(outputDir)) fs.delete(outputDir, true);

        FileInputFormat.addInputPath(job, inputDir);
        FileOutputFormat.setOutputPath(job, outputDir);

        for(int i = 0; i< numRegex; i++){
            MultipleOutputs.addNamedOutput(job, runHeader + "job" + i, TextOutputFormat.class, Text.class, IntWritable.class);
        }

        job.waitForCompletion(true);
    }

    private static void printHelp(){
        System.out.println("Usage : [input file] [output path] [number of regex] [regex] [regex] ...");
    }
}