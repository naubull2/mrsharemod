/*
 * Project : MRShareMod
 * Module : MRShareMod
 * Class : GrepReducer
 *
 * Author : Wooin Lee
 * Last edited : 15. 6. 23 ���� 11:09
 * wilee@kdd.snu.ac.kr
 */

package PseudoMRShare;

import Utils.TaggedKeyPair;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

import java.io.IOException;
import java.math.BigInteger;

public class GrepReducer extends Reducer<TaggedKeyPair, IntWritable, Text, IntWritable> {
    private int numRegex;
    private String header;
    private IntWritable sumValue = new IntWritable();
    private Text outKey = new Text();
    private MultipleOutputs<Text, IntWritable> mos;
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        // get total number of reg expressions
        Configuration conf = context.getConfiguration();
        numRegex = conf.getInt("numRegex", 1);
        header = conf.get("header");
        mos = new MultipleOutputs<Text, IntWritable>(context);
    }

    /**
     * Output multiple files per jobs (Each represented by a regular expression)
     * @param key a distinct word and its tag
     * @param values list of ones (1s)
     * @param context
     */
    @Override
    protected void reduce(TaggedKeyPair key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        // sum
        int sum = 0;
        for (IntWritable val : values) {
            sum += val.get();
        }
        sumValue.set(sum);
        outKey.set(key.getValue());

        // using multiple outputs, output according to the tag in the key
        for (int i = 0; i < numRegex; i++) {
            // check if i bit is set to 1
            if (BigInteger.valueOf(key.getTag()).testBit(i)) {
                String outputFileName = "output/out_"+i;
                mos.write(header + "job" + i, outKey, sumValue);
            }
        }
    }
    @Override
    protected void cleanup(Context context) throws IOException, InterruptedException {
        mos.close();
    }
}
