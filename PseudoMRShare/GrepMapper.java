/*
 * Project : MRShareMod
 * Module : MRShareMod
 * Class : GrepMapper
 *
 * Author : Wooin Lee
 * Last edited : 15. 6. 23 ���� 11:08
 * wilee@kdd.snu.ac.kr
 */

package PseudoMRShare;

import Utils.TaggedKeyPair;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.StringTokenizer;

public class GrepMapper extends Mapper<Object, Text, TaggedKeyPair, IntWritable> {
    private int numRegex;
    private String[] regex;
    private String token;
    private byte tag;
    private final byte ZERO = (byte) 0x00;
    private TaggedKeyPair outKey = new TaggedKeyPair();
    private IntWritable outValue = new IntWritable(1);
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        Configuration conf = context.getConfiguration();
        // receive regular expressions to be used
        numRegex = conf.getInt("numRegex", 1);
        regex = new String[numRegex];
        for(int i = 0; i < numRegex; i++)
            regex[i] = conf.get("regex."+i);
    }

    @Override
    protected void map(Object key, Text value, Context context) throws IOException, InterruptedException {
        StringTokenizer itr = new StringTokenizer(value.toString());
        while(itr.hasMoreTokens()){
            // Initialize tag to 0 bits
            tag = ZERO;
            token = itr.nextToken();
            // per regular expressions
            for(int i = 0; i < numRegex; i++) {
                if (token.matches(regex[i]))
                    tag = setTag(tag, i);
            }
            // output key + 8bit tag
            outKey.set(tag, token);
            if(tag != (byte)0x00)
                context.write(outKey, outValue);
        }
    }

    // Sets the [index] bit to 1
    private byte setTag(byte tag, int index){
        return (byte)(tag | (1 << index));
    }
}
